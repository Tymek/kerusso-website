# Kerusso - School of the Kingdom
For more informations check [Trello board].

## Setup
You need [Node.js] and [Git]. Then, from the command line:
```sh
$ git clone https://Tymek@bitbucket.org/Tymek/kerusso-website.git
$ cd kerusso-website
$ npm i
$ grunt
```
Should do the job. At least works for me ;) 

## Site configuration
Configuration variables are placed in `/php/config.php`.

## Alteration of the content
You can find HTML-like content in `/content/` folder. 
List of pages in use in order of appearance in menu is defined in `/php/config.php` file. 
Make sure, that file name of content page matches the name in config. 
For `style.less` development mode use command `grunt dev` or delete `/assets/style.css` and `/assets/style.css.map` files.

## Build source
When you finish making changes, run
```sh
$ grunt build
```
Result will be available in `kerusso.zip` file.

## Author
Tymoteusz Czech: <mail@tymek.cz>

**Please, don't contact me if you have problem using Git, Grunt or Bower.** Read manual first.

[Trello board]:https://trello.com/b/lcsJUSdW
[Node.js]:http://nodejs.org/
[Git]:http://git-scm.com/