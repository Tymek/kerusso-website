<?php
	include("php/functions.php");
?>
<!DOCTYPE html>
<html lang="<?php echo $page->language; ?>" itemscope itemprop="http://schema.org/<?php echo $page->schema; ?>">
<head>
<?php
	include("php/header.php"); 
?>
</head>
<body id="<?php echo $page->content; ?>">
<?php
	include("content/".$page->content.".php");
	include("php/footer.php");
?>
</body>
</html>
