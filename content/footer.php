<footer id="site-footer" itemscope itemtype="http://schema.org/School">
	<div class="container" itemscope itemtype="http://schema.org/PostalAddress">
		<div class="row">
			<div class="logo 
						hidden-xs
						hidden-sm
						col-md-2 
						col-lg-4">
				<div class="mask">
					<img src="<?php echo $page->path; ?>assets/images/footer-logo.png"
						alt="Kerusso logo">
				</div>
			</div>
			<div class="contact 
						col-xs-9 col-xs-offset-0 col-xs-push-0
						col-sm-5 col-sm-offset-1 col-sm-push-4
						col-md-5 col-md-offset-0 col-md-push-0
						col-lg-3 col-lg-offset-0 col-lg-push-0">
				Tel.: <span itemprop="telephone">+49.35873-36166</span><br>
				Fax: <span itemprop="faxNumber">+49.35873-36165</span><br>
				Email: <span itemprop="email">kingdomschool@bfwm.de</span><br>
			</div>
			<address class="hidden-xs
							col-sm-5 col-sm-offset-0 col-sm-pull-6
							col-md-4 col-md-offset-1 col-md-pull-0
							col-lg-4 col-lg-offset-1 col-lg-pull-0">
				<span itemprop="name">Youth With A Mission Herrnhut</span><br>
				<span itemprop="streetAddress">Untere Dorfstr. 56</span><br>
				<span itemprop="postalCode">02747</span> <span itemprop="addressLocality">Herrnhut</span><br>
				<span itemprop="addressCountry">Germany</span><br>
			</address>
		</div>
	</div>
</footer>
