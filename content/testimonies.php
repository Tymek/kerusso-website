<?php
	include ("php/top.php");
?>
<div id="content" role="main">
	<div class="container">
		<div class="page-header">
			<h1>Testimonies of our students</h1>
		</div>
		<section class="row" id="KelseyCallihan">
			<div class="col-xs-8 col-xs-offset-2 col-sm-5 col-md-4 col-md-offset-0">
				<figure class="img-t img-t1 pull-left">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/KelseyCallihan.jpg" alt="Kelsey Callihan" class="img-responsive">
					<figcaption>Kelsey Callihan <span>(USA)</span></figcaption>
				</figure>
			</div>
			<div class="clearfix visible-xs-block"></div>
			<div class="col-md-8 t1">
				<span class="mask mask1"></span><span class="mask mask2"></span><span class="mask mask3"></span><span class="mask mask4"></span><span class="mask mask5"></span><span class="mask mask6"></span><span class="mask mask7"></span><span class="mask mask8"></span><span class="mask mask9"></span><span class="mask mask10"></span><span class="mask mask11"></span><span class="mask mask12"></span><span class="mask mask13"></span><span class="mask mask14"></span><span class="mask mask15"></span><span class="mask mask16"></span><span class="mask mask17"></span><span class="mask mask18"></span><span class="mask mask19"></span><span class="mask mask20"></span><span class="mask mask21"></span><span class="mask mask22"></span><span class="mask mask23"></span><span class="mask mask24"></span><span class="mask mask25"></span><span class="mask mask26"></span><span class="mask mask27"></span><span class="mask mask28"></span><span class="mask mask29"></span><span class="mask mask30"></span>
				<p>When I first felt the Lord putting the Kerusso school on my heart &ndash; I&nbsp;was hesitant and unsure what to expect. Letting that hold me back from doing this school would have been my greatest mistake.</p>
				<p>Participating in Kerusso created an incredible platform to learn and hear from the Lord - dive deep into my identity as a daughter first - and strengthen my foundation in Him. As a student - you are brought into a rich community that is cultivated by a staff team who are so deeply committed to Jesus and Him being the absolute center.</p>
				<p>I was blown away by how the Lord met me during lecture phase - but outreach was another level. While in India, He opened my eyes to people - forever transforming my outlook and standard on loving others unconditionally. He freed me of fears I was carrying and replaced them with more of Himself. Honestly - I was shaken beyond what words could say and so much more confident in our relationship and my purpose because of it. I have walked away with practical tools for ministry, missions and living every day life with Jesus. If you’re looking for a greater depth of relationship with your Father and a further understanding of your role in the body of Christ, then you’ve found your launching pad here. I often tell people that participating in this school was the greatest choice I have ever made and I guarantee you'll be singing a similar tune.</p>
			</div>
		</section>
		<section class="row" id="Jimmy">
			<div class="col-md-12">
				<figure class="img-t img-t2 col-xs-10 col-xs-offset-1 col-md-5 col-sm-6 pull-right col-md-offset-0">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/Jimmy.jpg" alt="Jimmy" class="img-responsive">
					<figcaption>Jimmy <span>(Ethopia)</span></figcaption>
				</figure>
				<div class="clearfix visible-xs-block"></div>
				<p>I grew up where people were exposed to the Bible and its teachings but it never changed their lives, some believers begin well in the Christian life but for one reason or another they fail to mature in faith. I have learned that there is really something about the Kingdom that if God shows you the revelation then it changes your life! When you encounter the Kingdom of God you are never the same.</p>
				<p>By being in the Kingdom School I have learned to completely rely on God, not on my own strength or understanding. I have really learned what love is, to love God and to love people. I had worries about my intellect and provision but God showed me that when He calls you He will make a way for you.</p>
				<p>I used to struggle a lot with the fear of man and how people saw me but now I am confident in who I am in the Lord and I have a lot of boldness and freedom and something that I have also learned is that joy is different from happiness, Joy is not the absence of suffering, it is the presence of God. Matthew 6:33 &ldquo;Seek first the Kingdom of God and His righteousness and all the rest shall be given to you&rdquo;</p>
				<p>I did Kerusso to have an increase of faith and to find an unshakeable identity in Christ.</p>
			</div>
		</section>
		<section class="row" id="LeslieReuter">
			<div class="col-md-12">
				<figure class="img-t img-t3 col-xs-10 col-xs-offset-1 col-md-4 col-sm-5 pull-left col-md-offset-0">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/LeslieReuter.jpg" alt="Leslie Reuter" class="img-responsive">
					<figcaption>Leslie Reuter <span>(Germany)</span></figcaption>
				</figure>
				<p>Growing up in my church, I didn't know much about the kingdom, the supernatural, or the prophetic. But I knew there had to be more and I was eager to find out what that could mean in my life. </p>
				<p>Coming to Kerusso in 2013, I really didn't know what to expect.  All I knew was that I was hungry to meet God in a greater way. To say He exceeded my expectations is an understatement.</p>
				<p>The revelation of God being a good father is what struck me during the School. He is not comparable to any earthly father that could ever exist, and that brought me on a journey into His father heart. This lead me to discover my Identity as a daughter, and it changed my relationship with God completely. Because as a daughter in the Kingdom, I have access to whatever my father has and as I seek Him, the supernatural and the prophetic just become a byproduct of living life.</p>
				<p>Now I have a passion for others to enter into this same relationship. Jesus made a door for everyone to walk in the Kingdom and this invitation needs to get out!</p>
			</div>
		</section>
		<section class="row" id="GraceRhee">
			<div class="col-md-12">
				<figure class="img-t img-t4 col-xs-10 col-xs-offset-1 col-md-5 col-sm-6 pull-right col-md-offset-0">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/GraceRhee.jpg" alt="Grace Rhee" class="img-responsive">
					<figcaption>Grace Rhee <span>(USA/Korea)</span></figcaption>
				</figure>
				<p>It's so simple, it really is. But unless you truly grasp this, it's difficult to go much further. Do you know who you are? Do you really know Him? These are the questions that the Kingdom School challenged me with. This school makes you know that you know what you know.</p>
				<p>Beyond any lecture, any quote from a speaker, any journal entry in this eight weeks time, it has been the revelation in the core of my being that has utterly transformed my life- that I am a daughter of the King and I am a part of an unshakable Kingdom. Jesus tells us that the Kingdom is inside of us but few really understand what this means. This means that we have all the equipping that we need! Once we know our identity in Christ, there is nothing that can stand in the way of what God wants to do in and through us.</p>
				<p>I will spend the rest of my life diving deeper into Him, because in this season I have had a glimpse of how deep He goes and the true abundance of our life in the Kingdom.</p>
			</div>
		</section>
		<section class="row" id="BothwellMwedzi">
			<div class="col-md-12">
				<figure class="img-t img-t5 col-xs-6 col-xs-offset-3 col-md-3 col-sm-4 pull-left col-md-offset-0">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/BothwellMwedzi.jpg" alt="Bothwell Mwedzi" class="img-responsive">
					<figcaption>Bothwell Mwedzi <span>(Zimbabwe)</span></figcaption>
				</figure>
				<p>I think this school really gave me the opportunity open my eyes to the reality that God wants to partner with me, so in the process during the school knowing who I am as a son unleashed a confidence in identifying my gifts that were already there and walking in them and finding my unique place in His plan.</p>
				<p>My intimacy grew and it still is and it is from there that everything I live to testify comes from and thanks to this school for also changing my mind set, as Paul says Rom. 12:2.</p>
			</div>
		</section>
		<section class="row" id="KelseyAper">
			<div class="col-md-12">
				<figure class="img-t img-t6 col-xs-10 col-xs-offset-1 col-md-5 col-sm-6 pull-right col-md-offset-0">
					<img src="<?php echo $page->path; ?>assets/images/testimonies/KelseyAper.jpg" alt="Kelsey Aper" class="img-responsive">
					<figcaption>Kelsey Aper <span>(USA)</span></figcaption>
				</figure>
				<p>My experience during the Kingdom School was life-changing, to say the least.  Not only did I make life-long friendships, serve around the world, and hear amazing testimonies, but I learned more about Jesus and the Kingdom of Heaven than I ever anticipated.</p>
				<p>The teachings on the Holy Spirit, Prophetic Evangelism, and Kingdom Culture completely rocked my world.  I got to know my spiritual gifts, got baptized in the Holy Spirit, and had numerous supernatural encounters with the Lord.  Studying into different worldviews confirmed even deeper that a Biblical worldview is the only worthy worldview.  God set me free from false teachings, unclean spirits, and insecurities from the past.</p>
				<p>I feel more liberated, equipped, and prepared to PROCLAIM the Kingdom of God wherever I set foot.  The school taught me what it looks like to bring the Kingdom of Heaven to Earth, just the way Jesus did.  I am empowered to walk in the destiny of my unique calling, knowing that my true identity is found in Christ alone; that it doesn't matter what I think of myself, but only what God thinks of me.  I learned the importance of intimacy in relationship to God, and that all things flow out of that intimacy in the secret place with Him.</p>
				<p>I feel that the Kingdom School was where God molded me to be set apart.  I am now more passionate about the Kingdom, infatuated with the Word, and committed to Jesus than ever before, because I know God's Kingdom is my cause.</p>
			</div>
		</section>
	</div>
</div>
