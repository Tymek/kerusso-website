<?php
	include ("php/top.php");
?>
<div id="content" role="main">
	<section class="container">
		<div class="page-header">
			<h1>School admissions</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="list-group">
					<div class="list-group-item disabled cursor-reset">
							<h4 class="list-group-item-heading">DTS</h4>
							<p class="list-group-item-text">Everyone who would like to participate in Kerusso must first have completed their first entry course into YWAM.<br class="visible-md-inline visible-lg-inline"> 
							Once they have finished their Disciples Training School (DTS) they are welcome to apply.</p>
					</div>
					<div class="list-group-item">
							<h4 class="list-group-item-heading">Application fee</h4>
							<span class="badge">&euro;60</span>
							<p class="list-group-item-text">A one-time enrollment fee.</p>
					</div>
					<div class="list-group-item">
							<h4 class="list-group-item-heading">Lecture phase</h4>
							<span class="badge">&euro; 1100</span>
							<p class="list-group-item-text">8-week lecture phase in Herrnhut, Germany. Including housing and food.</p>
					</div>
					<div class="list-group-item">
							<h4 class="list-group-item-heading">Field assigment</h4>
							<span class="badge">&euro;1000-1600</span>
							<p class="list-group-item-text">Fees for the 6-week field assignment include airfares, housing and food.<br class="visible-md-inline visible-lg-inline"> 
							Depending on location (Africa, Asia or Eastern Europe).</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-9 col-lg-10">
				<p>We are excited to have you join us on a lifelong journey to see the King glorified in every single part of culture and&nbsp;society. Experience&nbsp;a&nbsp;life where the things that were impossible now become normal.</p>
			</div>
			<div class="col-sm-3 col-lg-2 text-xs-center"><a href="https://login.mission-live.com" class="btn btn-primary pull-right pull-xs-center skipline-xs" role="button">Apply online</a>
			</div>
		</div>
	</section>
</div>
