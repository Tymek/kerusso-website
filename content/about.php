<?php
	include ("php/top.php");
?>
<div id="content" role="main">
	<section class="container">
		<div class="page-header">
			<h1>About School of the Kingdom</h1>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p>Kerusso is a&nbsp;ministry training program affiliated with Youth With a&nbsp;Mission in Herrnhut, Germany. YWAM is a&nbsp;global movement of Christians from many cultures, age groups, and Christian traditions, dedicated to serving Jesus throughout the world. We unite in a&nbsp;common purpose to know God and to make Him known.</p>
				<img src="../assets/images/img2.jpg" alt="" class="img-responsive col-sm-5 img-left col-md-4">
				<p>School was birthed from a&nbsp;deep place of seeking God and the conviction that moving in the prophetic, supernatural evangelism, and the miraculous should be the every day life of a&nbsp;believer. With the Spirit thatraised Christ from the dead in each christian, we saw the need to create a&nbsp;place to be proactive in steppingout and putting our trust God.</p>
				<p>We are a&nbsp;team of people committed to walk in all Jesus paid for and who want to see others do the same. As we gather together, we see God move in marvelous and unexpected wayscreating a&nbsp;cycle of learning and increasing faith in Him for more. We won’t stop the earth is filled with theknowledge of the glory of the <span class="fake-small-caps">Lord</span>.</p>
			</div>
		</div>
		<div class="row col-md-12">
			<h2>Vision</h2>
			<p>In short Kerusso means: To Preach, To Proclaim, To be a&nbsp;Herald of the Good news. We believe a&nbsp;generation is be raised up, that will carry the heart of their King like never before. Those who will be bold enough to use their voice, as an ambassador of Christ, and represent the Kingdom of Heaven. a&nbsp;people who will feel whats on His heart, renew their mind to think His thoughts, and desire to see the world around them touched by God.</p>
			<p>The vision of Kerusso is to see these ambassadors walk passionately in love with Jesus and desire to take the transforming culture of the Kingdom into every sphere of life. Daily walking in identity and authority as Sons and Daughters in Gods Kingdom, doing all that Jesus told us is possible.</p>
			<blockquote>
				<p>&ldquo;Heal the sick, cleanse the lepers, raise the dead, cast out demons. Freely you have received, freely give.&rdquo;</p>
				<footer><cite>Matthew 10:7-8</cite></footer>
			</blockquote>
			<p>Our heart is to equip participants in the Word and the power of the Holy Spirit. Creating a&nbsp;place to cultivate a&nbsp;Kingdom mindset that empowers you to boldly proclaim Jesus, and see heaven invade this world through salvations, healings, signs and wonders. We provide a&nbsp;safe, loving, and dynamic environment to grow in Prophetic evangelism through learning to discern the voice of God with clarity and integrity. We want to demonstrate the truth of the Gospel of the Kingdom, by bringing transformation to the streets, cities and nations, all birthed out of worship, intercession and an intimate relationship with Jesus.</p>
			<blockquote>
				<p>&ldquo;For the Kingdom of God is not a&nbsp;matter of talk but of Power&rdquo;</p>
				<footer><cite>1 Corinthians 4:20</cite></footer>
			</blockquote>
			<p>Its a&nbsp;call to boldness, radical love, and a&nbsp;desire to join with God in seeing the reality of Heaven come to earth with every breath you take. There is a&nbsp;need to get this Gospel out to the nations. We get to be the ones to carry the light, where others don’t dare to go.</p>

			<h2>What we do</h2>
			<p>Kerusso emphasizes hands-on training and experience along with teaching and revelatory understanding. This creates a&nbsp;"do first, then teach" culture where all of the participants are expected to take risks to stretch their faith and grow in their experience with God.</p>
			<blockquote>
				<p>&ldquo;&hellip;to equip the saints for the work of ministry, for building up the body of Christ.&rdquo;</p>
				<footer><cite>Ephesians 4:12</cite></footer>
			</blockquote>

			<h2>What will training look like?</h2>
			<p>We have 8 weeks of life changing, biblical teaching and preaching. We want to create an environment for activations of the things imparted to you from a&nbsp;diverse range of dynamic speakers who live what they teach. We will focus on assimilating the core values of who God is, His nature and Character and your royal Identity in the Kingdom. The participants will learn how to function in the in the realm Spirit, all coming from Intimacy with jesus and creating a&nbsp;constant flow of the presence and power of God.</p>

			This training includes the following
			<ul>
				<li>The participants are required to read and dig deep into the Word of God. Not just reading the word but letting the Truth set them free and seeing every promise and of God as a&nbsp;possibility.</li>
				<li>Intimacy and time in Gods presence is a&nbsp;given as we want to see Him and minister to His heart before we go out to others.</li>
				<li>We will do local ministry each week in the community/city. We will begin going to the streets, on a&nbsp;hunt for those God has for us to speak to and release heaven over their life.</li>
				<li>Moving in the prophetic, Sharing the Gospel, praying for the sick will become a&nbsp;daily lifestyle as you engage in reaching out wherever you go.</li>
				<li>Everyone will choose a&nbsp;revival and a&nbsp;revivalist from history and investigate them with a&nbsp;view what we can learn from them, how to apply similar dynamics in our own lives.</li>
				<li>The entire school will also go on a&nbsp;missions faith week were they will trust the Lord for food, accommodation, transport and be led by the Holy Spirit to minister to people.</li>
			</ul>

			<h2>Outreach</h2>
			<p>After the training portions, you be be part of a&nbsp;ministry team for a&nbsp;radical and intense 6-8 weeks of outreach to the nations. Where we will be preaching, teaching the gospel and extending the Kingdom of God. Our heart is to see lives impacted and letting God transform you through the journey by being Jesus’s hands, heart and feet to those who need healing, love, deliverance, salvation and compassion. From the past outreaches we have seen many people have personal encounters with Christ through prophetic insights, healings, salvations , deliverances and experiencing the presence of God resulting in them giving their hearts to God. </p>
		</div>
	</section>
</div>
