<div class="top cover">
	<div class="container navbar" id="top" role="navigation">
		<a href="#menu" class="pull-right menu-button" title="Menu">
			<img src="assets/images/menu.svg" onerror="this.src='assets/images/menu.png'" alt="menu" class="menu-icon">
		</a>
	</div>
	<div id="logo-container" class="center-container">
		<img src="assets/images/logo-white.svg" onerror="this.src='assets/images/logo-white.png'" alt="Kerusso - School of the Kingdom" id="logo" class="logo logo-white center-block vertical-center">
	</div>
	<nav id="menu-container" class="center-container off">
		<?php
			include ("php/menu.php");
		?>
	</nav>
</div>
<div id="content" role="main">
	<section class="container">
		<div class="row">
			<div class="col-md-5 col-md-padding">
				<h2>To preach and proclaim the Gospel of the Kingdom</h2>
				<p class="ending">Kerusso is a fast-paced season of intense training. It's a call to boldness, radical love, and a desire to join with God in seeing the reality of Heaven released by living a supernatural life style everywhere you go. It’s were we  move from merely echoing a message to becoming a message.</p>
				<p>Read more at:</p>
				<a href="about/" class="btn btn-primary space" role="button">Our Vision</a>
				<a href="admissions/" class="btn btn-primary" role="button">Addmissions</a>
			</div>
			<div class="col-md-6 col-md-offset-1"><img src="assets/images/img4.jpg" alt="On outreach" class="img-responsive"></div>
		</div>
	</section>
	<section class="container testimonies">
		<h2>Testimonies</h2>
		<div class="row">
			<div class="col-md-6 col-md-padding">
				<a href="<?php echo $page->path; ?>testimonies/#KelseyCallihan" class="quote-panel">
					<div class="row">
						<div class="col-xs-3 col-sm-2 col-md-4">
							<img src="assets/images/testimonies/thumbnails/KelseyCallihan.jpg" alt="Kelsey" class="img-responsive">
						</div>
						<div class="col-xs-8 col-sm-9 col-md-7">
							<blockquote>I often tell people that participating in this school was the greatest choice I have ever made and I guarantee you'll be singing a similar tune!
							<footer>Kelsey</footer>
							</blockquote>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-6 col-md-padding">
				<a href="<?php echo $page->path; ?>testimonies/#Jimmy" class="quote-panel">
					<div class="row">
						<div class="col-xs-3 col-sm-2 col-md-4">
							<img src="assets/images/testimonies/thumbnails/Jimmy.jpg" alt="Jimmy" class="img-responsive">
						</div>
						<div class="col-xs-8 col-sm-9 col-md-7">
							<blockquote>I have learned that there is really something about the Kingdom that if God shows you the revelation then it changes your life!
							<footer>Jimmy</footer>
							</blockquote>
						</div>
					</div>
				</a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-md-padding">
				<a href="<?php echo $page->path; ?>testimonies/#BothwellMwedzi" class="quote-panel">
					<div class="row">
						<div class="col-xs-3 col-sm-2 col-md-4">
							<img src="assets/images/testimonies/thumbnails/BothwellMwedzi.jpg" alt="Bothwell" class="img-responsive">
						</div>
						<div class="col-xs-8 col-sm-9 col-md-7">
							<blockquote>I think this school really gave me the opportunity open my eyes to the reality that God wants to partner with me, &hellip;
							<footer>Bothwell</footer>
							</blockquote>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-6 col-md-padding">
				<a href="<?php echo $page->path; ?>testimonies/#GraceRhee" class="quote-panel">
					<div class="row">
						<div class="col-xs-3 col-sm-2 col-md-4">
							<img src="assets/images/testimonies/thumbnails/GraceRhee.jpg" alt="Grace" class="img-responsive">
						</div>
						<div class="col-xs-8 col-sm-9 col-md-7">
							<blockquote>I will spend the rest of my life diving deeper into Him, because in this season I have had a glimpse of how deep He goes and the true abundance of our life in the Kingdom.
							<footer>Grace</footer>
							</blockquote>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-6 col-md-padding">
			</div>
		</div>
		<div class="row text-center">
			<div class="row-md-12">
				<a href="testimonies/" class="btn btn-primary" role="button">More testimonies</a>
			</div>
		</div>
	</section>
	<section id="next-shift">
		<div class="container">
			<div class="row">
				<h2 class="col-md-12">Next shift</h2>
			</div>
		</div>
		<div class="bg-map-europe">
			<div class="container">
				<div class="row">
					<div class="col-md-12 heading">
						<span class="when">April 2nd, 2016</span>
						<span class="where">
							Herrnhut
							<img src="assets/images/marker.svg" alt="map marker" class="marker">
							Germany
						</span>
						<a href="https://login.mission-live.com" class="btn btn-primary" role="button">Apply online</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
