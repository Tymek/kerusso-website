<?php
	include ("php/top.php");
?>
<div id="content" role="main">
	<section class="container">
		<div class="page-header">
			<h1>Contact</h1>
		</div>

		<div class="row">
			<div class="col-md-6">
				<form class="row" name="contactform" method="post" action="<?php $page->path; ?>../php/send.php">
					<fieldset class="form-horizontal col-md-11 col-md-offset-1" role="form">
						<legend class="col-md-12">Write to us</legend>
						<div class="form-group">
							<label for="email" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="email" name="email" required>
							</div>
						</div>
						<div class="form-group">
							<label for="username" class="col-sm-2 control-label">Name</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="username" name="username" required>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="message">Message</label>
							<div class="controls">
								<div class="col-sm-10">
									<textarea id="message" name="message" class="form-control" rows="3"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<div class="g-recaptcha" data-sitekey="<?php echo RECAPTCHA_KEY; ?>"></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button id="submit" type="submit" class="btn btn-primary" role="button" data-loading-state="Sending&hellip;" data-content="Send">Send</button>
							</div>
						</div>
					</fieldset>
				</form>
				<!-- / FORM -->
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-11 col-md-offset-1">
						<h2>Visit our location</h2>
						<p>You're welcome to visit us any time. Just contact us if you find yourself nearby. See you in Herrnhut!</p>
						<div id="map-canvas" class="img-thumbnail"></div>
						<a href="https://www.google.com/maps/place/Jugend+Mit+Einer+Mission/@50.3583677,15.7522055,6z/data=!4m2!3m1!1s0x47091eca4aae0a75:0x1b3cb4f52f03e8cd"><span class="glyphicon glyphicon-resize-full" aria-hidden="true"></span> Full screen map</a>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
