module.exports = function (grunt) {

//	var autoprefixer = require('autoprefixer-core');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		concat: {
			jquery: {
				options: {
					sourceMap: true,
				},
				files: {
					'assets/jquery.js': ['bower_components/jquery/jquery.min.js']
						//,'assets/jquery.min.map': ['bower_components/jquery/jquery.min.map']
				}
			},
			bootstrapjs: {
				files: {
					'assets/bootstrap.js': ['bower_components/bootstrap/dist/js/bootstrap.min.js']
				}
			},
			autosize: {
				options: {
					sourceMap: true,
				},
				files: {
					'assets/autosize.js': ['bower_components/autosize/src/autosize.js']
				}
			},
		},
		copy: {
			fonts: {
				expand: true,
				cwd: 'bower_components/bootstrap/fonts/',
				src: '**',
				dest: 'assets/fonts/',
				flatten: true,
				filter: 'isFile',
			}
		},
		"bower-install-simple": {
			options: {
				color: true,
				directory: "bower_components"
			},
			"prod": {
				options: {
					production: true
				}
			},
			"dev": {
				options: {
					production: false
				}
			}
		},
		less: {
			build: {
				options: {
					paths: ["assets/css"]
					/*modifyVars: {
						imgPath: '"http://mycdn.com/path/to/images"',
						bgColor: 'red'
					}*/
				},
				files: {
					"assets/style.css": "assets/style.less"
				}
			}
		},
		autoprefixer: {
			build: {
				options: {
					browsers: ['> 5%', 'last 2 versions', 'ie 8', 'ie 9']
				},
				src: 'assets/style.css'
			},
		},
		cssmin: {
			options: {
				roundingPrecision: -1,
				compatibility: 'ie8',
				keepBreaks: false,
				shorthandCompacting: false,
				report: 'gzip',
				sourceMap: true
			},
			target: {
				files: {
					'assets/style.css': ['assets/style.css']
				}
			}
		},
		clean: {
  			css: ['assets/style.css', 'assets/style.css.map'],
  			archive: ['../kerusso.zip']
		},
		compress: {
			main: {
				options: {
					archive: 'kerusso.zip'
				},
				files: [
					{
						expand: true,
						cwd: './',
						src: ['*.php'
							, '*.txt'
							, '*.png'
							, '*.jpg'
							, '.htaccess'
							, '*.ico'
							, 'assets/**'
							, '!assets/*.less'
							, 'php/**'
							, 'content/**'
							 ]
					}
				]
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-bower-install-simple');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-compress');
	grunt.registerTask('default', [
		'bower-install-simple',
		'copy',
		'concat'
	]);
	grunt.registerTask('dev', [
		'clean:css'
	]);
	grunt.registerTask('build', [
		'less:build',
		'autoprefixer:build',
		'cssmin',
		'compress'
	]);

};