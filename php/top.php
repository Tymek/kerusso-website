<div class="top secondary" role="banner">
	<div class="container">
		<nav class="row">
			<div class="col-md-4 col-xs-8">
				<a href="<?php echo $page->path; ?>">
					<img src="<?php echo $page->path; ?>assets/images/logo-alt.svg" onerror="this.src='<?php echo $page->path; ?>assets/images/logo-alt.png'" alt="Logo" class="menu-logo-alt img-responsive">
				</a>
			</div>
			<div class="col-xs-4 hidden-md hidden-lg">
				<a href="#menu">
					<img src="<?php echo $page->path; ?>assets/images/menu.svg" onerror="this.src='<?php echo $page->path; ?>assets/images/menu.png'" alt="menu" class="menu-icon pull-right">
				</a>
			</div>
			<div class="col-xs-12 col-md-6 col-md-offset-2 relative dropdown">
				<?php
					include "menu.php";
				?>
			</div>
		</nav>
	</div>
</div>
