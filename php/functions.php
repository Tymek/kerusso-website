<?php
require_once 'config.php';

$page = (object) array();
if (isSet($_GET["page"])){
	$page->request = htmlspecialchars($_GET["page"]);
	$page->id = -1;
	$i = -1;
	foreach ($pages as &$value){
		$i++;
		if ($value != $pages[0] && strtolower($value) === $page->request){
			$page->id = $i;
			$page->content = $page->request;
			break;
		};
	}
	if ($page->id == -1){
		$page->content = "404";
		header("HTTP/1.0 404 Not Found");
	}
} else {
	$page->id = 0;
	$page->content = "home";
}
unset ($value);

$page->title = ($page->id == 0) ? HOME_TITLE : $pages[$page->id]." &ndash; ".SITE_TITLE;
$page->language = LANGUAGE;
$page->tracking = ANALYTICS;
$page->path = ($page->id == 0) ? "" : "../";

$page->canonical = "";
$page->home = "";
if($_SERVER["HTTP_HOST"] != "localhost"){
	$page->canonical = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : (!empty($_SERVER['ORIG_PATH_INFO']) ? $_SERVER['ORIG_PATH_INFO'] : '');
	$page->canonical = "http://".$_SERVER["HTTP_HOST"].str_replace("index.php","",$page->canonical);
	$page->home = $page->canonical;
	$page->canonical .= ($page->content != "home")?$page->content."/":"";
}

switch($page->content){
	case "about":
		$page->schema = "AboutPage";
		break;
	case "contact":
		$page->schema = "ContactPage";
		break;
	default:
		$page->schema = "WebPage";
}


?>
