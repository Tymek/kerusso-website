<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="favicon.ico">
<title><?php echo $page->title; ?></title>
<?php if($page->home != "" && $page->content == "home"): ?>
<meta itemprop="image" content="<?php echo $page->home; ?>assets/images/logo.jpg">
<meta meta property="og:image" content="<?php echo $page->home; ?>assets/images/og-big.jpg">
<meta meta property="og:image" content="<?php echo $page->home; ?>assets/images/og-small.jpg">
<?php endif; /*?>
<link rel="canonical" href="<?php echo $page->canonical; ?>">
<?php*/ if(file_exists("assets/style.css")): ?>
<link rel="stylesheet" type="text/css" href="<?php echo $page->path; ?>assets/style.css">
<?php else: ?>
<link rel="stylesheet/less" type="text/css" href="<?php echo $page->path; ?>assets/style.less">
<script>
	less = {
		env: "development",
		async: false,
		fileAsync: false,
		poll: 1000,
		functions: {},
		dumpLineNumbers: "comments",
		relativeUrls: false,
		rootpath: "<?php echo $page->path; ?>assets/"
		//rootpath: "localhost/assets/"
	};
</script>
<script src="<?php echo $page->path; ?>bower_components/less/dist/less.min.js" type="text/javascript"></script>
<script>less.watch();</script>
<?php endif; ?>
<link rel="author" href="<?php echo $page->path; ?>humans.txt" />
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
