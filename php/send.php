<?php
require_once("config.php");

if(!defined("DEBUG")){
	define("DEBUG", FALSE);
}

if(!DEBUG){
	if(!is_ajax()){
		header("HTTP/1.0 403 Forbidden");
	}
	header('Content-Type: application/json');
}

try {
	$response['success'] = TRUE;
	
	if(DEBUG){
		var_dump($_SERVER);
		var_dump($_POST);
	}
	
	if(!isSet($_POST['email'])
	|| !isSet($_POST['username'])
	|| !isSet($_POST['message'])
	|| !isSet($_POST['g-recaptcha-response'])
	) {
		throw new Exception("Data incomplete.");
	}
	
	// EMAIL verification
	$email = $_POST['email'];
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		throw new Exception("Email address invalid.");
	}
	
	// NAME verification
	$name = $_POST['username'];
	$name = filter_var($name, FILTER_SANITIZE_STRING);
	if(empty($name)){
		throw new Exception("Enter your name.");
	}
	
	// MESSAGE verification
	$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
	
	
	// CAPTCHA verification
	$captcha = $_POST['g-recaptcha-response'];
	$captcha = filter_var($captcha, FILTER_SANITIZE_STRING);
	
	$captcha = "https://www.google.com/recaptcha/api/siteverify?response=".$captcha;
	$captcha.= "&secret=".RECAPTCHA_SECRET_KEY;
	$captcha.= "&remoteip=".$_SERVER['REMOTE_ADDR'];
	
	$captcha = file_get_contents($captcha);
	$captcha = json_decode($captcha, true);
	if(DEBUG){
		var_dump($captcha);
	}
	if (!$captcha['success']) {
		throw new Exception("<strong>Verification failed!</strong> Captcha invalid.");
	}
	
	$content = "Name: ".clean_str($name)."\n";
	$content .= "Email: ".clean_str($email)."\n";
	if(!empty($message)){
		$content .= "\nMessage:\n".clean_str($message)."\n";
	}

	$headers = 'From: '.$name."<".$email.">\r\n".
	'Reply-To: '.$email."\r\n" .
	'X-Mailer: PHP/' . phpversion();

	// SEND MESSAGE
	if(! mail(MAIL_TARGET, MAIL_SUBJECT, $content, $headers)) {
		throw new Exception("Server error. Please try again later.");
	}
	
	$response['success'] = TRUE;
	
} catch (Exception $e) {
	$response['success'] = FALSE;
	$response['error'] = $e->getMessage();
}

// Output
echo json_encode($response);

function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

function clean_str($string) {
	$bad = array("content-type","bcc:","to:","cc:","href");
	return str_replace($bad,"",$string);
}

?>
