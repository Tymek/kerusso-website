<?php
	define("HOME_TITLE", "Kerusso &ndash; School of the Kingdom");
	define("SITE_TITLE", "Kerusso");
	define("LANGUAGE", "en");
	
	define("ANALYTICS", "UA-20465854-4");
	
	define("MAIL_TARGET", "kingdomschool@bfwm.de");
	define("MAIL_SUBJECT", SITE_TITLE." - Contact form submited");
	define("RECAPTCHA_KEY", "6LdxGQMTAAAAAGg0CBM-2U-II6hGLRephKRCPOdC");
	define("RECAPTCHA_SECRET_KEY", "6LdxGQMTAAAAAFE3ejDWth7idAYQ3L8fOdNiF5IT");

	$pages = array (
		"Home",
		"About",
		"Admissions",
		"Testimonies",
		"Contact"
	);

	define("DEBUG", false);
?>