/*global $, jQuery, google, grecaptcha, console, autosize, document*/


function gmaps_load() {
	"use strict";
	var script = document.createElement('script');
	script.type = 'text/javascript';
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=gmaps_init&language=en';
	document.body.appendChild(script);
}

function gmaps_init() {
	"use strict";
	var mapOptions = {
		zoom: 5,
		streetViewControl: false,
		center: new google.maps.LatLng(51.00624, 14.7424)
	}, map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions),
		marker = new google.maps.Marker({
			position: mapOptions.center,
			map: map,
			title: "YWAM Herrnhut"
		});
}

function send_mail() {
	"use strict";
	function reset_form() {
		$("#submit").removeProp("disabled").html(function () {
			return $(this).data("content");
		});
		grecaptcha.reset();
	}
	function clear_form() {
		reset_form();
		$("input").val("");
		$("textarea").text("");
	}
	function write_alert(content, status) {
		$("form legend").addClass("hidden");
		if (!$("form alert").length) {
			$("form fieldset").prepend("<div class=\"alert\"></div>");
			$("form fieldset .alert").addClass("col-md-12");
		}
		var $alert = $("form .alert");
		$alert.removeClass("alert-*")
			.addClass("alert-" + status)
			.html(content);
	}
	$("#submit").prop("disabled", true).html(function () {
		return $(this).data("loading-state");
	});
	$.post(
		$("form").attr("action"),
		$("form").serialize(),
		function (data, textStatus) {
			if (data.success) {
				clear_form();
				write_alert("<strong>Thank you!</strong> We will reply soon.", "success");
			} else {
				write_alert(data.error, "warning");
				reset_form();
			}
		},
		"json"
	).fail(function () {
		reset_form();
		write_alert("<strong>Server error.</strong> Please try again later.", "danger");
	});
}

$(function () {
	"use strict";
	$("a[href=#menu]").click(function (e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$(".center-container").toggleClass("off");
		$("body").toggleClass("menu-open");
	});
	if ($("body").attr("id") === "contact") {
		gmaps_load();
		autosize($('textarea'));
		$("form").submit(function (e) {
			e.preventDefault();
			send_mail();
		});
	}
});
